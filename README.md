# zip能够压缩与解压缩Zip格式的压缩档（WinZip、PKZIP）；且能对此类类档案进行处理，包括产生压缩档、列出压缩档的内容以及解压 缩档案等等。由于能够在伺服器端进行压缩与解压缩的动作，所以相当方便使用。
PclZip定义一个PclZip类别，其类别物件可视为一个ZIP档案，亦提供method来进行处理。

## Overview

A fast and simple streaming zip file downloader for PHP. Using this library will save you from having to write the Zip to disk. You can directly send it to the user, which is much faster. It can work with S3 buckets or any PSR7 Stream.

## Installation

Simply add a dependency on laoqianjunzi/zip to your project's composer.json file if you use Composer to manage the dependencies of your project. Use following command to add the package to your project's dependencies:

```bash
composer require laoqianjunzi/zip
```

## Usage and options

Here's a simple example:

```php
// Autoload the dependencies
require 'vendor/autoload.php';

// enable output of HTTP headers
$options = new zip\Option\Archive();
$options->setSendHttpHeaders(true);

// create a new zip object
$zip = new zip\zip('example.zip', $options);

// create a file named 'hello.txt'
$zip->addFile('hello.txt', 'This is the contents of hello.txt');

// add a file named 'some_image.jpg' from a local file 'path/to/image.jpg'
$zip->addFileFromPath('some_image.jpg', 'path/to/image.jpg');

// add a file named 'goodbye.txt' from an open stream resource
$fp = tmpfile();
fwrite($fp, 'The quick brown fox jumped over the lazy dog.');
rewind($fp);
$zip->addFileFromStream('goodbye.txt', $fp);
fclose($fp);

// finish the zip stream
$zip->finish();
```

You can also add comments, modify file timestamps, and customize (or
disable) the HTTP headers. It is also possible to specify the storage method when adding files,
the current default storage method is 'deflate' i.e files are stored with Compression mode 0x08.

