<?php
declare(strict_types=1);

namespace zip;

/**
 * This class is only for inheriting
 */
abstract class Exception extends \Exception
{
}
