<?php
declare(strict_types=1);

namespace zip\Exception;

use zip\Exception;

/**
 * This Exception gets invoked if file or comment encoding is incorrect
 */
class EncodingException extends Exception
{
}
