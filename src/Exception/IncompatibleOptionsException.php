<?php
declare(strict_types=1);

namespace zip\Exception;

use zip\Exception;

/**
 * This Exception gets invoked if options are incompatible
 */
class IncompatibleOptionsException extends Exception
{
}
